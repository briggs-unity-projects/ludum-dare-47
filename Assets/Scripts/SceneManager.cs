﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{

    public enum TaskType
    {
        NOTHING,
        POUR_DRINK,
        CLEAN
    }

    public struct StaffTask
    {
        public StaffTask(TaskType taskType, GameObject target)
        {
            this.taskType = taskType;
            this.target = target;
        }
        public TaskType taskType;
        public GameObject target;
    }


    public List<TableManager> tableManagers;

    public bool spareTable;

    public GameObject customerPrefab = null;
    private bool waitingForCustomers = false;

    public List<GameObject> customerSpawnLocations;

    public Queue<StaffTask> staffTasks = new Queue<StaffTask>();

    public GameObject exit;

    private void Awake()
    {
        StartCoroutine(SendInCustomers(UnityEngine.Random.Range(1, 1)));
    }
    // Update is called once per frame
    void Update()
    {
        // Check if spare table exists
        CheckTableExists();

        // Check for new customers
        CheckForNewCustomers();
    }


    private void CheckTableExists()
    {
        if (tableManagers.Exists(a => a.isTaken == false))
        {
            spareTable = true;
        }
        else
        {
            spareTable = false;
        }
    }

    private void CheckForNewCustomers()
    {
        if (spareTable && !waitingForCustomers)
        {
            waitingForCustomers = true;
            StartCoroutine(SendInCustomers(UnityEngine.Random.Range(10, 20)));
        }
    }

    IEnumerator SendInCustomers(float time)
    {
        yield return new WaitForSeconds(time);

        // Get first available table
        TableManager table = tableManagers.Find(a => a.isTaken == false);
        table.SetTaken(true);
        
        // Spawn customers now!
        // Decide how many customers to spawn ( 1 - 4 )
        int totalCustomers = UnityEngine.Random.Range(1, 5);
        table.SetTotalMembers(totalCustomers);
        for(int i = 0; i < totalCustomers; i++)
        {
            GameObject obj =
                Instantiate(customerPrefab, customerSpawnLocations[i].transform.position,
                    Quaternion.identity, transform);
            CustomerMovement mv = obj.GetComponent<CustomerMovement>();
            mv.SetDestination(table.seats[i].transform);
            mv.SetTable(table);
        }

        // Set waiting for custmers back to false for next drunks!
        waitingForCustomers = false;

    }

    public void CreateTask(StaffTask staffTask)
    {
        staffTasks.Enqueue(staffTask);
    }

    public StaffTask GetNextTask()
    {
        if (staffTasks.Count != 0)
        {
            return staffTasks.Dequeue();
        }
        else
        {
            return new StaffTask(TaskType.NOTHING, null);
        }
    }

    public GameObject GetExit()
    {
        return exit;
    }
}
