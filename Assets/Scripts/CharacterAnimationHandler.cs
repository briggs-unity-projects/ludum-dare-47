﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationHandler : MonoBehaviour
{
    Animator animator;
    Rigidbody rigidBody;

    Vector3 lastPosition;
    // Use this for initialization
    void Start()
    {
        lastPosition = this.transform.position;
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 curPos = this.transform.position;
        if (curPos == lastPosition)
            animator.SetBool("IsMoving", false);
        else
            animator.SetBool("IsMoving", true);

        lastPosition = curPos;
        
    }
}
