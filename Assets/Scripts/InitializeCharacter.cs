﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InitializeCharacter : MonoBehaviour
{
    public List<Material> skinTones;
    public Renderer actualSkinTone;

    public List<Material> shirtColors;
    public Renderer actualShirtColor;

    public GameObject glassesPosition;
    public List<GameObject> glasses;
    public GameObject hatPosition;
    public List<GameObject> hats;

    // Start is called before the first frame update
    void Start()
    {
        // Set skin tone
        actualSkinTone.material = skinTones.ElementAt(UnityEngine.Random.Range(0, skinTones.Count));
        // Set shirt colour
        actualShirtColor.material = shirtColors.ElementAt(UnityEngine.Random.Range(0, shirtColors.Count));

        // Select glasses
        // 50/50 wearing glasses
        if(UnityEngine.Random.Range(0, 4) == 0)
        {
            int glassesIndex = UnityEngine.Random.Range(0, glasses.Count);
            GameObject go;
            go = Instantiate(glasses.ElementAt(glassesIndex), glassesPosition.transform.position, Quaternion.identity) as GameObject;
            go.transform.parent = glassesPosition.transform;
            go.transform.localRotation = Quaternion.identity;
            
        }

        // Select hat
        int hatIndex = UnityEngine.Random.Range(0, hats.Count);
        if (hatIndex != 0)
        {
            GameObject go;
            go = Instantiate(hats.ElementAt(hatIndex), hatPosition.transform.position, Quaternion.identity) as GameObject;
            go.transform.parent = hatPosition.transform;
            go.transform.localRotation = Quaternion.identity;
        }
    }

}
