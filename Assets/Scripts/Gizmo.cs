﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gizmo : MonoBehaviour
{

    public enum GizmoType
    {
        Cube,
        Sphere
    }

    public GizmoType gizmoType;

    public Color color;

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        if(gizmoType == GizmoType.Sphere)
            Gizmos.DrawSphere(transform.position, 1);
        if (gizmoType == GizmoType.Cube)
            Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));
    }
#endif
}
