﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableManager : MonoBehaviour
{

    public bool isTaken = false;
    public Light tableLight = null;
    public List<GameObject> seats;
    public int totalMembers = 0;

    SceneManager sceneManager;

    // Start is called before the first frame update
    void Start()
    {
        sceneManager = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<SceneManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isTaken)
        {
            tableLight.enabled = true;
            if(totalMembers == 0)
            {
                isTaken = false;
            }
        }
        else
        {
            tableLight.enabled = false;
        }
    }

    public void SetTaken(bool isTaken)
    {
        this.isTaken = isTaken;
    }

    public void SetTotalMembers(int totalMembers)
    {
        this.totalMembers = totalMembers;
    }

    public int GetTotalMembers()
    {
        return totalMembers;
    }

    public void RemoveMemberFromTable()
    {
        totalMembers = totalMembers - 1;
        sceneManager.CreateTask(new SceneManager.StaffTask(SceneManager.TaskType.CLEAN, this.gameObject));
    }
}
