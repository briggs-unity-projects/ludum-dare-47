﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CustomerMovement : MonoBehaviour
{
    public enum State
    {
        ENTER,
        SEAT_WAITING,
        SEAT_DRINKING,
        LEAVING
    }

    NavMeshAgent navMeshAgent;

    public State state = State.ENTER;
    public Transform target;

    public float targetDistance = 0.5f;
    public float distanceToTarget = 0f;

    SceneManager sceneManager;

    TableManager table;

    public bool boolStartedDrinking = false;
    public float minDrinkingTime = 30f;
    public float maxDrinkingTime = 60f;

    public GameObject mask;
    public GameObject pint;

    Animator animator;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        if (navMeshAgent == null)
        {
            Debug.LogError("Nav mesh agent not attached to " + gameObject.name);
        }

        sceneManager = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<SceneManager>();

    }

    public void SetDestination(Transform destination)
    {
        target = destination;
        navMeshAgent.SetDestination(destination.position);
        navMeshAgent.isStopped = false;
    }

    public void SetTable(TableManager table)
    {
        this.table = table;
    }

    public void Update()
    {
        // On enter check how close you are to seat
        if (state == State.ENTER)
        {
            distanceToTarget = Vector3.Distance(transform.position, target.position);
            if (distanceToTarget < targetDistance)
            {
                // Stop navmesh
                navMeshAgent.isStopped = true;
                // Update transform
                transform.position = target.position;
                transform.rotation = target.rotation;
                // Update state and order a pint!
                state = State.SEAT_WAITING;
                sceneManager.CreateTask(new SceneManager.StaffTask(SceneManager.TaskType.POUR_DRINK, this.gameObject));
                // Take off mask!
                mask.SetActive(false);
            }
        }
        else if (state == State.SEAT_DRINKING && !boolStartedDrinking)
        {
            // Drink animation and count down till either next pint or leave!
            Debug.Log("Started drinking");
            boolStartedDrinking = true;
            StartCoroutine(DrinkPint());
        }
        else if (state == State.LEAVING)
        {
            distanceToTarget = Vector3.Distance(transform.position, target.position);
            // Leave!
            if (distanceToTarget < targetDistance)
            {
                Destroy(gameObject);
            }
        }

    }

    public IEnumerator DrinkPint()
    {
        pint.SetActive(true);
        animator.SetBool("isDrinking", true);
        yield return new WaitForSeconds(Random.Range(minDrinkingTime, maxDrinkingTime));
        pint.SetActive(false);
        animator.SetBool("isDrinking", false);
        // Decide on another pint or leave
        boolStartedDrinking = false;
        if(Random.Range(0, table.GetTotalMembers()) == 0)
        {
            // Leave
            SetupLeave();
            // Ask for another drink
            SetupAnotherPint();

        }
    }

    private void SetupLeave()
    {
        SetDestination(sceneManager.GetExit().transform);
        table.RemoveMemberFromTable();
        // Put on mask!
        mask.SetActive(true);
        state = State.LEAVING;
    }

    private void SetupAnotherPint()
    {
        state = State.SEAT_WAITING;
        sceneManager.CreateTask(new SceneManager.StaffTask(SceneManager.TaskType.POUR_DRINK, this.gameObject));
    }

}
