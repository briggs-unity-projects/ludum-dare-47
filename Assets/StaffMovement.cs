﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StaffMovement : MonoBehaviour
{
    NavMeshAgent navMeshAgent;

    public Queue<GameObject> targets = new Queue<GameObject>();
    public GameObject currentTarget;

    public SceneManager.StaffTask currentTask;

    SceneManager sceneManager;

    public float targetDistance = 0.5f;
    public float distanceToTarget = 0f;

    public float pintPouringTime = 2f;

    public bool holdingPint = false;

    public GameObject pint;
    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        // set current task as nothing
        currentTask = new SceneManager.StaffTask(SceneManager.TaskType.NOTHING, null);
        // Get scene manager
        sceneManager = GameObject.FindGameObjectWithTag("SceneManager").GetComponent<SceneManager>();
    }


    // Update is called once per frame
    void Update()
    {
        pint.SetActive(holdingPint);
        if (currentTask.taskType == SceneManager.TaskType.NOTHING)
        {
            var task = sceneManager.GetNextTask();
            if (task.taskType != SceneManager.TaskType.NOTHING)
            {
                currentTask = task;
                SetupTask(currentTask);
            }
        }
        else if (currentTask.taskType == SceneManager.TaskType.POUR_DRINK)
        {
            distanceToTarget = Vector3.Distance(transform.position, currentTarget.transform.position);
            if (distanceToTarget < targetDistance)
            {
                // Stop navmesh
                navMeshAgent.isStopped = true;
                // Is pouring pint or giving pint
                if(targets.Count > 0 && !holdingPint)
                {
                    // Pouring pint
                    holdingPint = true;
                    StartCoroutine(PourPint());
                }

                if (targets.Count == 0 && holdingPint)
                {
                    // Give pint 
                    holdingPint = false;
                    CustomerMovement customer = currentTarget.GetComponent<CustomerMovement>();
                    customer.state = CustomerMovement.State.SEAT_DRINKING;
                    // set current task as nothing
                    currentTask = new SceneManager.StaffTask(SceneManager.TaskType.NOTHING, null);
                }
            }
        }
        else
        {
            currentTask = new SceneManager.StaffTask(SceneManager.TaskType.NOTHING, null);
        }



    }

    // Set destination
    public void SetDestination(GameObject destination)
    {
        currentTarget = destination;
        navMeshAgent.SetDestination(destination.transform.position);
        navMeshAgent.isStopped = false;
    }

    // Setup task
    public void SetupTask(SceneManager.StaffTask staffTask)
    {
        if(staffTask.taskType == SceneManager.TaskType.POUR_DRINK)
        {
            // Get all beer taps and choose random one!
            var taps = GameObject.FindGameObjectsWithTag("BeerTap");
            // Queue beer tap then person
            targets.Enqueue(taps[Random.Range(0, taps.Length)]);
            targets.Enqueue(staffTask.target);
            // Set destination
            SetDestination(targets.Dequeue());
        }
    }

    public IEnumerator PourPint()
    {
        yield return new WaitForSeconds(pintPouringTime);

        // Set new desintation whilst holding pint
        SetDestination(targets.Dequeue());

    }
}
